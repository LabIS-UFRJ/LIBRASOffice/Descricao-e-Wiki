# LIBRASOffice
----

## O que é?
Visite o [site](https://librasoffice.cos.ufrj.br/)

No Brasil, há 9,7 milhões de pessoas com deficiência auditiva e um pouco mais de 2 milhões com deficiência auditiva severa, segundo o Censo 2010. São números expressivos que representam uma comunidade que encara inúmeras dificuldades no processo de adaptação a um mundo voltado para pessoas não surdas. Para uma pessoa surda, a primeira língua é a LIBRAS (Língua Brasileira de Sinais) e mesmo sendo uma das línguas oficiais do Brasil, majoritariamente os dispositivos eletrônicos, como celulares e computadores, não possuem suporte suficiente para LIBRAS. 

Em 2015, a partir de uma demanda interna da UFRJ para a formação de funcionários terceirizados surdos, o projeto LIBRASOffice começou a ser pensado e desenvolvido como projeto experimental de fim de curso da disciplina Computadores e Sociedade, oferecida pela linha de pesquisa em Informática e Sociedade do PESC/COPPE ao curso de Engenharia de Computação e Informação da Universidade Federal do Rio de Janeiro (UFRJ). Seu desenvolvimento seguiu posteriormente a cargo do [Laboratório de Informática e Sociedade](https://is.cos.ufrj.br/labis/) (LabIS), projeto de extensão desta linha de pesquisa, caracterizado por seu compromisso com um Brasil mais igualitário, por sua interdisciplinaridade e pela sua formação de graduandos e pós-graduandos fundamentada na indissociabilidade do ensino-pesquisa-extensão.

O LIBRASOffice é um projeto de código aberto, cujo objetivo é a adaptação da suíte de escritório LibreOffice, também de código aberto, para pessoas surdas, a partir do desenvolvimento de uma interface que traduz as funcionalidades do LibreOffice para LIBRAS. Através de uma janela que se abre no canto inferior direito da tela do LibreOffice,  são apresentados, por meio de vídeos pré-gravados,  os sinais referentes a cada ícone, item de menu ou de submenu na medida em que vão sendo selecionados pelo usuário ou usuária. Resumidamente, o LIBRASOffice é uma tecnologia assistiva, cuja interface de acessibilidade incorpora a LIBRAS ao pacote de escritório LibreOffice (a segunda maior distribuição de softwares para escritório do mundo). 

Desde o início de seu desenvolvimento, membros da comunidade surda brasileira têm oferecido suporte tanto ao processo de gravação de sinais como aos testes de usabilidade do LIBRASOffice. Suas críticas e sugestões vêm sendo continuamente incorporadas, garantindo um nível de qualidade e amadurecimento ao LIBRASOffice que autoriza o seu presente lançamento a um público maior de forma a estender seus benefícios em prol da integração digital da população surda. E, mesmo com o isolamento social decorrente da pandemia de COVID-19, e contando também com o apoio da comunidade brasileira de Software Livre, foi possível desenvolver a presente versão de lançamento do LIBRASOffice. 

Mais informações podem ser obtidas através do [site do projeto](https://librasoffice.cos.ufrj.br/), onde é possível realizar o download do software, receber comentários e sugestões (inclusive de novos sinais), e entrar em contato com a equipe de desenvolvimento. 

## Requisitos para instalação

**Requisitos gerais**
- [Java SE Development Kit 11](https://www.oracle.com/technetwork/java/javase/downloads/jdk11-downloads-5066655.html)
- Sistema operacional Windows ou GNU/Linux x64 

**Para Windows**

- Microsoft Windows XP, Vista, Windows 7, ou Windows 8;
- PC compatível com Pentium (são recomendados Pentium III, Athlon ou mais recentes);
- 256 MB RAM (recomendada 512 MB RAM);
- Até 1.5 GB de espaço disponível em disco;
- 1024x768 de resolução (quanto maior for melhor), com pelo menos 256 cores.
São necessários direitos de administrador para o processo de instalação. 
É recomendada a boa prática de fazer uma cópia de segurança do seu sistema e dados antes de instalar ou remover programas.

**Para GNU/Linux**

- Linux kernel versão 2.6.18 ou superior;
- glibc2 versão 2.5 ou superior;
- gtk versão 2.10.4 ou superior;
- PC compatível com Pentium (recomenda-se Pentium III, Athlon ou mai recentes);
- 256MB RAM (recomendada 512MB RAM);
- Até 1.55GB de espaço disponível em disco;
- X Server com resolução de 1024x768 (quanto mais alta melhor), com pelo menos 256 cores;
- Gnome 2.16 ou superior, com os pacotes gail 1.8.6 e at-spi 1.7 
(requeridos para suportar ferramentas de tecnologia de assistência [AT]), ou outro GUI compatível (tal como KDE, entre outros).

Para algumas funcionalidades do programa - mas não muitas - é necessário o Java. O Java é especialmente necessário para o Base.

## Organização do repositório

Repositório "[LASOBack](https://gitlab.com/LabIS-UFRJ/LIBRASOffice/LASOBack)": Clone do repositório core do LibreOffice 5.3.7.2 (LO) utilizado pelo LIBRASOffice (LASO). No clone são realizadas modificações em alguns módulos do LO, a nível de código-fonte C++, para a implementação do LASO.

Repositório "[LASOFront](https://gitlab.com/LabIS-UFRJ/LIBRASOffice/LASOFront)": Código-fonte Java do LIBRASOffice que utliza o LASOBack para algumas funções e a API UNO do LibreOffice para outras.

## Entendendo o LIBRASOffice
----
> O algoritmo funciona da seguinte maneira:

> - Identificar da rotina de renderização das "janelinhas de ajuda" (tooltip/helptext). **(Backend)**

> - Extrair texto a ser exibido na janelinha para um arquivo de Log. **(Backend)**

> - Capturar linha de log, analisar e exibir sinal correspondente. **(Frontend)**

Este processo foi estabelecido de forma a poder ser repetido em outros programas.

## Glossário
----

Esta página guarda um glossário dos termos e abreviações conceituais e técnicas utilizados no projeto.

- ECI: curso de graduação em Engenharia de Computação e Informação da UFRJ, acesse [aqui](http://www.poli.ufrj.br/graduacao_cursos_engenharia_computacao_informacao.php.) 

- LASO: abreviação de LIBRASOffice

- LO: abreveviação LibreOffice, acesse [aqui](https://pt-br.libreoffice.org)

- lode: LibreOffice Development Environment, acesse [aqui](https://wiki.documentfoundation.org/Development/lode)

- LIpE: acrônimo para Laboratório de Informática para Educação da UFRJ

- UFRJ: sigla para Universidade Federal do Rio de Janeiro, acesse [aqui](https://ufrj.br)

## changelog LIBRASOffice
* 01-Out-2019 por [Lidiana](https://gitlab.com/Lidiana)
* 03-Abr-2021 por [Lidiana](https://gitlab.com/Lidiana)

